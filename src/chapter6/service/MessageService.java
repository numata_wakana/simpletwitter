package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId,String startDay,String endDay) {
        final int LIMIT_NUM = 1000;
        Connection connection = null;
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat time = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	    String start;
	    String end;
        try {
        	if(! StringUtils.isBlank(startDay) ) {
        		start = startDay + " 00:00:00";

        	} else {
	        	start = "2020-01-01 00:00:00";
        	}
        	if(! StringUtils.isBlank(endDay)) {
        		end = endDay + " 23:59:59";

        	} else {
        		end = time.format(timestamp);
        	}
            connection = getConnection();
            Integer id = null;
            if(! StringUtils.isBlank(userId) && userId.matches("^[0-9]*$")) {
            	id = Integer.parseInt(userId);
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, LIMIT_NUM, start, end, id );
            commit(connection);

            return messages;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(int id) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, id);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public Message selectMessage(String messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            Message message = new MessageDao().select(connection, messageId);
            commit(connection);

            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}