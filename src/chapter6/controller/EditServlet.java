package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet("/edit")
public class EditServlet extends HttpServlet {

	@Override
	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {

		 String messageId = request.getParameter("id");
		 Message message = new MessageService().selectMessage(messageId);

		 if(message == null) {
			 request.getSession().setAttribute("errorMessages","不正なパラメータが入力されました");
			 response.sendRedirect("./");

		 } else {
			 request.setAttribute("message", message);
			 request.getRequestDispatcher("/edit.jsp").forward(request, response);
		 }
	}

	 @Override
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {

		 HttpSession session = request.getSession();
		 String text = request.getParameter("text");
		 int messageId =Integer.parseInt(request.getParameter("id"));

		 List<String> errorMessages = new ArrayList<String>();

		 if (!isValid(text, errorMessages)) {
	            session.setAttribute("errorMessages", errorMessages);
	            request.getRequestDispatcher("/edit.jsp").forward(request, response);
	            return;
		 }
		 Message message = new Message();
	     message.setText(text);
	     message.setId(messageId);

		 new MessageService().update(message);
		 response.sendRedirect("./");
	 }

	  private boolean isValid(String text, List<String> errorMessages) {

	        if (StringUtils.isBlank(text)) {
	            errorMessages.add("入力してください");
	        } else if (140 < text.length()) {
	            errorMessages.add("140文字以下で入力してください");
	        }
	        if (errorMessages.size() != 0) {
	            return false;
	        }
	        return true;
	    }
}
